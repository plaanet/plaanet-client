export default {
   meta: {
      code: 'en',
      name: 'English',
   },

   auth: {
      emptyCredentials: 'Credentials cannot be empty',
      invalidCredentials: 'Invalid credentials',
   },


   instances: {
      unreachableNode: 'The server is unreachable'
   },

   main: {
      cancel: 'cancel',
      Cancel: 'Cancel',
      delete: 'delete',
      Delete: 'Delete',
      edit: 'edit',
      Edit: 'Edit',
      EditDelete: 'edit, delete',
      save: 'save',
      validate: 'validate',
      Validate: 'Validate',
      password: 'Password',
      login: 'Login',
      register: 'Create your account',
      back: 'back',
      knowMore: 'know more',
      user: 'user',
      users: 'users',
      server: 'server',
      Server: 'Server',
      Confidentiality: 'Confidentiality',
      mentionLegal: 'Legales',
      follower: 'follower', 
      followers: 'followers',
      About: 'About',
      Finished: 'Ok',
      Contacts: 'Contacts',
      Messages: 'Messages',
      New: 'New',
      Image: 'Image',
      MyProfil: 'My profil',
      Create: 'Create'
   },

   menu: {
      searchContact: 'Search contact',
      Stream: 'Stream',
      Home: 'Home',
      CreatePage: 'Create page',
      MyPages: 'My pages',
      MyFollows: 'Following',
      IAmMember: 'I am member',
      MyContacts: 'My contacts',
      Blacklist: 'Blacklist',
      HomePage: 'Home page',
      Logout: 'Logout',
      PrivateMessaging: 'Private messages',
      Notif: 'Notifications',
      Settings: 'Preferences',
      MyProfile: 'My profile',
      StatVisitors: 'Stat visitors',
      PageModo: 'Admin pages',
      VersionMobile: 'Mobile App Version',
      Admin: 'Admin',
      Updates: 'Updates',
      EmailSettings: 'Emails',
   },


   home: {
      mainTitle1: 'Social network for social crisis',
      geocom: 'Geo-communication',
      servingRebellion: 'serving rebellion',
      connectAccount: 'connect to your account',
      pseudoFormLogin: 'Pseudo / username',
      youAreConnected: 'You are connected to a server',
      theManifest: 'The Manifest',
      theCharter: 'Charter',
      geoComInfo: 'Geo-communication',
      joinRebellion: 'Create my account',
      readTheManifest: 'Read the manifest',
      TheManifest: 'The manifest',
      switchServer: 'Switch server',
      clear: 'clear',
      bootNode: 'boot node',
      clearConnexionData: 'Clear data connexion',
      clearConnexionDataBtn: 'Clear data',
      decentralization: 'Decentralization',
      networkStatus: 'Network status',
      selectAServer: 'Select a server',
      visit: 'Quick view'
   },

   stream: {
      searchContent: 'search content...',
      showMap: 'Show map',
      showResultOnMap: 'Show results on map',
      sendPost: 'Send post',
      searchUser: 'Contacts',
      selectSourceStream: 'Select the stream source',
      localStream: 'Local stream',
      favoritePosts: 'Favorite posts',
      myFollows: 'I follow',
      noMessage: 'No message',
      noResult: 'No result',
      endOfStream: 'End of stream',
      endOfResults: 'End of results',
      refresh: 'Refresh',
      newsstream: 'Stream',
      position: 'Position',
      searchInStream: 'search in stream',
      searchInPages: 'search in pages',
      hashtagSuggestions: '#hashtags suggestions',
      update: 'update',
      results: 'results',
      result: 'result',
      limit: 'limit',
      kmByDay: 'km/day',
      used: 'used',
      remaining: 'remaining',
      remainingTitle: 'You can still use',
      today: 'today',
      youAlreadyUsed: 'you already used',
      PublishAs: 'Publish as',
      YourMessage: 'Your message...',
      msgUsedAllKm: 'Sorry, you already used all of your km for today. You will be able to post messages again this evening, starting at 12:00 am.',
      enableShare: 'Allow to share this message',
      addPicture: 'picture',
      searchAPlace: 'search a city',
      searchAPage: 'search a page',
      searchOnMap: 'search on map',
      pages: 'pages',
      comment: 'comment',
      comments: 'comments'
   },

   post: {
      publishedAMessage: 'published a message',
      sharedAMessage: 'shared a message',
      from: 'from',
      edit: 'Edit post',
      delete: 'Delete post',
      leaveConversation: 'Leave conversation',
      yourComment: 'Your comment',

      dyw_follow: 'Do you want to follow the activity of',
      dyw_follow_sub: 'Retreive all his post by selecting the "follow" option as stream source',
      dyw_delete_post: 'Do you realy want to delete this post',
      dyw_addto_blacklist1: 'Do you want to add',
      dyw_addto_blacklist2: 'to your blacklist',
      dyw_addto_addToContact: 'to your contact list',
      dyw_addto_addToContact_sub: 'Enable private messaging with this contact',
      dyw_addto_blacklist_sub: 'You will no longer receive any of its posts',
      dyw_to_remove_this_page: 'Do you realy want to delete the page',
      
      uAllreadyAre_follow: 'You are already following',
      uAllreadyAre_contact: 'is already in your contact list',
      uAllreadyAre_blacklist: 'is already in your blacklist',

      dyw_follow_cancel: 'Do you want to stop following',
      dyw_contact_cancel: 'Do you want to remove this contact',
      dyw_blacklist_cancel: 'Do you want to this contact from your blacklist ?',

      yesFollow: 'Yes, follow',
      yesCancelFollow: 'Stop following',
      yesBlackL: 'Yes, hide',
      yesCancelBlackL: 'Yes, hide',
      yesAddToContact: 'Add to my contacts',
      yesRemoveFromContact: 'Remove this contact',
      yesDeletePage: 'Yes, delete the page',

      documentDeleted: 'This document has been deleted by the author'
   },

   comment: {
      answers: 'answers',
      yourAnswer: 'Your answer',

      dyw_delete_comment: 'Do you realy want to delete this comment'
   },

   map: {
      selectReceptionZone: 'Select the reception area',
      clickToSend: 'then click send',
      peopleAffected: 'people affected',
      Send: 'Send',
      SendingYourMessage: 'Sending your message',
      PleaseWait: 'Please wait',
      seePage: 'Show the page',
      privateMsg: 'Private message',
      notLocated: 'not located'
   },

   page: {
      follow: 'follow',
      blacklist: 'blacklist',
      contact: 'contact',
      addToContact: 'add to your contacts',
      FurtherInfo: 'Further informations',
      DescInfo: 'Write a description to give more information about the purpose of this page',
      saveDesc: 'Save description',
      mainPosition: 'Main position of this page',
      files: 'Files',
      removeAFile: 'Remove a file',
      sureToRemoveAFile: 'Are you sure you want to delete this file ?',
      noFile: 'No file',
      deleteFile: 'Delete this file',
      tags: 'tags',
      MsgSendOnlyToFollowers: 'This message will be sent only to your followers',
      MsgSendToFollowersAndShare1: 'This message will first be sent to your subscribers',
      MsgSendToFollowersAndShare2: 'They can then share it around them to make it travel',
      SendMsgToFollowers: 'Send a message to your subscribers',
      loadingLinkPreview: 'Loading link preview',
      SelectImg: 'Select image',
      ImgSelected: 'Selected image',
      changeImgProfil: 'Change my avatar',

      
      role_admin: 'administrator',
      role_admin_short: 'administrator',
      role_moderator: 'moderator',
      role_editor: 'member',
      friend: 'friend',
      becomeFriend: 'Become friend',
      youAreFriend: 'You are friends',
      youAreMember: "You are member",

   },

   selectNode: {
      isANetwork: 'is a',
      decentralized: 'decentralised network',
      desc11: 'To enter the network',
      desc12: 'please indicate the name of a city',
      desc13: 'You will be automatically logged in',
      desc14: 'to the nearest server',
      findClosestNode: 'Find the node closest to you',
      whereDYL: 'Where do you live',
      selectServerChoice: 'or select the server you want',
      showServers: 'Show server list',
      optimPerf: 'In order to optimize network performance',
      useClosestServer: 'it is recommended to use the server closest to you',
      selectServer: 'Select a server',
      selectThisServer: 'select this server',
   },

   register: {
      didNotSignedManifest: 'You have not yet signed the manifest',
      readSignedManifest: 'Read and sign the manifest',
      manifestSigned: 'Manifest signed',
      createYourAccount: 'Create your account',

      desc11: 'Register now and start using Plaanet in a few seconds',
      desc12: 'No need to give any e-mail adresse',
      desc13: 'just a pseudo and a password',
      desc14: 'We don\'t want to know who you are',
      desc15: 'We only want to know what we can do together',

      alreadyHaveAccount: "Already have an account"
   },

   login: {
      connectToAccount: 'Connect',
      name: 'Pseudo, username',
      errorData: 'Sorry, incorrect connexion data',
      errorUserNotExists: 'Sorry, this user account does not exists in this server',
      errorUserPageNotExists: 'Sorry, the page associated to your user account does not exists in this server',
      errorWrongPwd: 'Sorry, this account exists, but it is a wrong password',
      errorServerUnreachable: 'Sorry, the server is unreachable',

   },

   form: {
      required: 'Required',
      characters: 'characters',
      noSpace: 'No space',
      atLeast8Char: 'At least 8 characters',
      atLeast3Char: 'At least 3 characters',
   },

   createPage: {
      createANewPage: 'Create a new page',
      whatKindActivity: 'What kind of page do you want to create',
      pageName: "Name of this page",
      name: 'Name',
      aboutThisPage: 'What about this page',
      Description: 'Description',
   },
   
   createPageType: {
      group: 'Create a group',
      event: 'Create an event',
      assembly: 'Create an assembly',
   },

   createSurvey: {
      organizeVote: 'Organize a votation',
      organizeSeanceVote: 'Organize a votation',
      publish_survey: 'Publish a survey',
      publish_referendum: 'Publish a referendum',
      publishThe_survey: 'Publish survey',
      publishThe_referendum: 'Publish referendum',
      whatKindSurvey: 'What kind of survey do you want to publish',
      surveyTitle: 'Title / Question',
      mainText: 'Text',
      answer: 'answer'
   },

   survey: {
      type_undefined: 'survey',
      type_survey: 'survey',
      type_referendum: 'referendum',
      type_a_survey: 'a survey',
      type_a_referendum: 'referendum',
      typeEdition_publish: 'Publish',
      typeEdition_edit: 'Edit',
      typeEdition_duplicate: 'Duplicate',
      dyw_delete_survey: 'Do you realy want to delete this survey',
      
   },

   pageType: {
      user: 'User',
      group: 'Group',
      event: 'Event',
      assembly: 'Assembly',
      friend: 'Friend',
   },

   private: {
      PrivateMessaging: 'Private messaging',
      Send: 'Send',
      Picture: 'Picture',
      AddPicture: 'Add a picture',
      HastagsSuggestions: 'hashtags suggestions',
      SendPrivateMessage: 'Send a private message',
      AddRecipients: 'Add recipients',
      AllowAddingRecipients: 'Allow adding recipients',
      YourMessage: 'Your message',
      FindRecipients: 'Find recipients',
      noSearchResult: 'No results were found for your search',
      noContactResult: 'No results found in your contacts',
      ExtendedSearch: 'Extended search',
      ContactListEmpty: 'Your contacts list is empty',
      UseTheButton: 'Use the button',
      toAddMember: 'to add a member to your contacts',
   },
   carousel: {
      next: 'Next',
      prev: 'Previous',
      ariaLabel: {
         delimiter: ' ',
      }
   },
   
   error: {
      CredentialsAreInvalid : 'Credentials are invalid',
      errorData: 'Sorry, incorrect connexion data',
      errorUserNotExists: 'Sorry, this user account does not exists in this server',
      errorUserPageNotExists: 'Sorry, the page associated to your user account does not exists in this server',
      errorWrongPwd: 'Sorry, this account exists, but it is a wrong password',
      errorServerUnreachable: 'Sorry, the server is unreachable',
      
   },

   live: {
      editLiveMsg: 'Edit this message',
      deleteLiveMsg: 'Delete this message',

   },

   noDataText: 'No data available',
}
