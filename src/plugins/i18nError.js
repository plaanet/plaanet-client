const I18NError = {
  install(Vue) {
    Vue.prototype.$i18nError = ($vuetify, { i18nKey }) => {
      return $vuetify.lang.t(`$vuetify.${i18nKey}`);
    }
  },
};

export default I18NError;