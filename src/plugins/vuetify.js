import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';

// Translation provided by Vuetify (typescript)
//import en from './locale/en'
//import fr from 'vuetify/src/locale/fr.ts'
import fr from './translation/fr.ts'
import en from './translation/en.ts'

// Your own translation file
//import fr from './plugins/translation/fr'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            light: {
                primary: colors.cyan.darken2,
                secondary: colors.teal.lighten1,
                accent: colors.shades.black,
                error: colors.red.accent3,
            },
            dark: {
                
                primary: colors.cyan.darken2,
                secondary: colors.teal.lighten1,
                accent: colors.shades.black,
                error: colors.red.accent3,
                danger: "#372c29",
                darkblock: "#212121"
            },
        },
    },
    lang: {
        locales: { fr, en },
        current: 'fr',
        default: 'en'
    },
    
});
