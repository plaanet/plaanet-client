const axios = require('axios');

const state = () => ({
  showLinkPreviews: false,
  linkPreviewsLoading: false,
  linkPreviews: {},
});

const actions = {
  async addLinkPreview({ commit, state }, url) {
    // Dont waste resources if we already have the preview.
    if (Object.keys(state.linkPreviews).includes(url)) {
      return;
    }

    try {
      commit("setLinkPreviewsLoading", true);

      const { data: oembedReq } = await axios.get(`/autocomplete/oembed?url=${encodeURIComponent(url)}`);
      if (oembedReq.success) {
        return commit("addLinkPreview", {
          url,
          metas: oembedReq.data.oembed
        });
      }else{
        try {
          const { data: ogReq } = await axios.get(`/autocomplete/opengraph?url=${encodeURIComponent(url)}`);
          if (ogReq.success) {
            return commit("addLinkPreview", {
              url,
              metas: ogReq.data.opengraph
            });
          }
        } catch (err2) {
          if (window.isDev) {
            console.error(`Cannot retrieve preview for link ${url}. Error:`, err2);
          }
        }
      }
    } catch (err) {
      try {
        const { data: ogReq } = await axios.get(`/autocomplete/opengraph?url=${encodeURIComponent(url)}`);
        if (ogReq.success) {
          return commit("addLinkPreview", {
            url,
            metas: ogReq.data.opengraph
          });
        }
      } catch (err2) {
        if (window.isDev) {
          console.error(`Cannot retrieve preview for link ${url}. Error:`, err2);
        }
      }
    } finally {
      commit('setLinkPreviewsLoading', false);
    }
  },
  removeLinkPreview({ commit }, url) {
    commit('removeLinkPreviewByUrl', url);
  },
  showLinkPreviews({ commit }) {
    commit('setShowLinkPreviews', true);
  },
  hideLinkPreviews({ commit }) {
    commit('setShowLinkPreviews', false);
    commit('clearLinkPreviews');
  }
};

const mutations = {
  addLinkPreview(state, { url, metas }) {
    state.linkPreviews = {
      ...state.linkPreviews,
      [url]: metas,
    };
  },
  removeLinkPreviewByUrl(state, url) {
    // eslint-disable-next-line no-unused-vars
    const { [url]: curr, ...previews } = state.linkPreviews;
    state.linkPreviews = previews;
  },
  setShowLinkPreviews(state, show) {
    state.showLinkPreviews = show;
  },
  setLinkPreviewsLoading(state, isLoading) {
    state.linkPreviewsLoading = isLoading;
  },
  clearLinkPreviews(state) {
    state.linkPreviews = {};
  },
};

const getters = {
  shouldShowLinkPreviews(state) {
    if (state.linkPreviewsLoading) {
      return false;
    }

    return state.showLinkPreviews;
  },
  linkPreviewsList(state) {
    return Object.values(state.linkPreviews);
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}