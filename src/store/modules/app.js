import axios from "axios";

const state = () => ({
  isLoading: true,
  drawerOpen: true,
  popupEmailOpen: false,
  popupScopeOpen: false,

  activity: {
    pages: [],
    assemblies: [],
    events: [],
  }
});

const actions = {
  setDrawerOpen({ commit }, open) {
    commit("setDrawerState", open);
  },
  setAppLoading({ commit }, isLoading) {
    commit('setLoadingState', isLoading);
  },
  setPopupEmailOpen({ commit }, isOpen) {
    commit('setPopupEmailOpen', isOpen);
  },
  setPopupScopeOpen({ commit }, isOpen) {
    commit('setPopupScopeOpen', isOpen);
  },
  async fetchActivity({ commit }, params) {
    await commit('setActivity', []);
    let key = params.key
    axios.post('/page/get-map-pages', params)
          .then((res)=>{
            commit('setActivity', { key: key, pages: res.data.mapPages });
          })

  }
};

const mutations = {
  setDrawerState(state, open) {
    state.drawerOpen = open;
  },
  setLoadingState(state, isLoading) {
    state.isLoading = isLoading;
  },
  setPopupEmailOpen(state, isOpen) {
    state.popupEmailOpen = isOpen;
  },
  setPopupScopeOpen(state, isOpen) {
    state.popupScopeOpen = isOpen;
  },
  setActivity(state, data) {
    console.log("state.activity", state.activity)
    console.log("setActivity", data.key, data)
    state.activity[data.key] = data.pages
  }
};

const getters = {};

export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
};
