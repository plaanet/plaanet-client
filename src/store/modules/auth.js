
import axios from "axios";

import core from "../../plugins/core";
import router from "../../router/router";
const config = require('../../config/' + process.env.NODE_ENV)

// initial state

const state = () => ({
  login: {
    success: false,
    error: null,
    loading: false,
  },
  user: {
    id: null,
    name: null,
    access_token: null
  },
  profile: {
    uid: null,
    name: null,
    pages: null,
    avatar: null,
    step: null,
    follows: [],
    lat: 46.16685851396646,
    lng: -1.1697435379028323,
  },
  radius: 600000,
  referrer: {
    profile: null,
    error: null,
    loading: false,
  },
  register: {
    success: false,
    error: null,
    loading: false,
  },
  isLoggedIn: false,
  isAdmin: false,
  avatarUrl: '',
  isNewUser: false,
  loginErrorMsg: '',
  lastCheckLiveNotif: new Date(),
  countUnreadLiveMsg: 0,
})

// getters
const getters = {
  hasReferrerUser: (state) => { state.referrer.profile != null },
  
}

// actions
const actions = {
  async checkAuth({ dispatch, commit, state }) {
    try {
      if (state.isLoggedIn) {
        return;
      }

      const res = await axios.get("/auth/whoami");
      const { success, data, error } = res.data;

      // TODO: Handle this better, use the store.
      // TODO: Display something to the user? Redirect to /login?
      if (!success) {
        if (window.isDev) {
          console.log("Cannot check login state. Err:", error.message);
        }

        router.push('/logout');
        return;
      }

      if (data.isLoggedIn) {
        if (window.isDev) {
          console.log(`Currently logged in as ${data.username} (id: ${data._id})`);
        }

        await dispatch('auth/fetchUserContext', null, { root: true });
        await dispatch('notifications/fetchNotifications', null, { root: true });
        setTimeout(async ()=>{ 
          await dispatch('notifications/fetchInviteRoles', null, { root: true })
          await dispatch('notifications/fetchPendingRoles', null, { root: true })
        }, 5000)
      }
    } catch (err) {
      commit('logout');
    } finally {
      await dispatch("app/setAppLoading", false, { root: true });
    }
  },
  async fetchUserContext({ commit }) {
    try {
      const res = await axios.get("/user/context");
      const { success, error, data } = res.data;

      // TODO: Handle this better, use the store.
      // TODO: Display something to the user? Redirect to /login?
      if (!success) {
        throw new Error("Cannot retrieve user context. Err:", error.message);
      }

      const avatarUrl = core.avatarUrl(data.userPage.uid) + "?" + Math.random()
      const userContext = {
        user: data.user,
        profile: data.userPage,
        isNewUser: data.isNewUser,
        memberPages: data.memberPages,
        avatarUrl: avatarUrl,
      };

      if (window.isDev) {
        console.log('Fetched user context:', userContext);
      }

      commit("login", userContext);
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot fetch user profile:', err.message);
      }

      commit('logout');
    }
  },
  async loginUser({ dispatch, commit }, credentials) {
    try {
      commit('fetchLoginLoading', true);

      const { data } = await axios.post('/auth/login', {
        name: credentials.name,
        password: credentials.password,
        mobileAppVersion: config.mobileAppVersion,
        mobileAppName: config.mobileAppName
      });

      if (!data.success) {
        commit("fetchLoginError", data.error);
        return;
      }

      await dispatch('auth/fetchUserContext', null, { root: true });
      await dispatch('notifications/fetchNotifications', null, { root: true });
      commit('fetchLoginSuccess');
    } catch (err) {
      console.log("User cannot be authenticated. Error:", err);

      commit("fetchLoginError", {
        name: 'NetworkError',
        message: 'Cannot join instance',
        i18nKey: 'instances.unreachableNode'
      });
      commit('logout');
    } finally {
      commit('fetchLoginLoading', false);
    }
  },
  async register({ commit }, params) {
    try {
      commit('fetchRegisterLoading', true);

      const { credentials, referrerUid } = params;

      const { data } = await axios.post('/auth/register', {
        username: credentials.username,
        password: credentials.password,
        referrerUid: referrerUid,
      });

      if (data.success === false) {
        commit("fetchRegisterError", data.error);
        return;
      }

      commit('fetchRegisterSuccess');
    } catch (err) {
      console.log('ERROR: User cannot be registered. Error:', err);
    } finally {
      commit('fetchRegisterLoading', false);
    }
  },
  changeAvatar({ commit }) {
    commit('changeAvatar')
  },
  login({ dispatch, commit }, data) {
    commit('login', {
      user: data.user,
      profile: data.userPage,
      isNewUser: data.isNewUser,
      memberPages: data.memberPages,
      avatarUrl: core.avatarUrl(data.userPage.uid) + "?" + Math.random(),
    });

    commit('update_position', {
      lat: data.userPage.coordinates[1],
      lng: data.userPage.coordinates[0],
    });

    // Open drawer.
    dispatch('app/setDrawerOpen', true, { root: true });
  },
  async logout({ dispatch, commit }) {
    try {
      await axios.post('/auth/logout');
      
      dispatch("app/setDrawerOpen", true, { root: true });
      commit('logout')

      //vide le stream
      dispatch("stream/initPosts", true, { root: true });
      dispatch("stream/initPages", true, { root: true });

      router.replace('/');
    } catch (err) {
      console.error('CANNOT LOGOUT O-O')
    }
  },
  async fetchReferrerUser({ commit }, referrerUid) {
    try {
      commit('fetchReferrerLoading', true);

      const { data } = await axios.get(`/user/profile/${referrerUid}`);
      if (!data.success) {
        throw new Error(data.error);
      }

      commit("fetchReferrerSuccess", data.data.profile);
    } catch (err) {
      commit('fetchReferrerError', err);
    } finally {
      commit('fetchReferrerLoading', false);
    }
  },
  change_user_step(context, step) {
    context.commit('change_user_step', step)
  },
  change_admin_state(context, bool){
    context.commit('change_admin_state', bool)
  },
  add_page_to_user(context, page) {
    context.commit('add_page_to_user', page)
  },
  update_position(context, geoloc){
    context.commit('update_position', geoloc)
  },
  update_city_address(context, data){
    context.commit('update_city_address', data)
  },
  update_hashtags(context, hashtags){
    context.commit('update_hashtags', hashtags)
  },
  update_current_radius(context, radius){
    context.commit('update_current_radius', radius)
  },
  update_date_read_notif(context, date){
    if (window.isDev) {
      console.log("inna update_date_read_notif", date);
    }

    context.commit('update_date_read_notif', date)
  },
  update_date_read_private(context, date){
    context.commit('update_date_read_private', date)
  },
  update_is_new_user(context){
    context.commit('update_is_new_user', true)
  },

  alertMemberNewLivemsg(context, data){
    context.commit('alert_member_new_livemsg', data)
  },
}


// mutations
const mutations = {
  login(state, context) {
    let mPages = core.sortMember(context.memberPages)

    state.user = context.user
    state.profile = context.profile
    state.profile.memberPages = mPages
    state.isAdmin = context.user.isAdmin
    state.isLoggedIn = true
    state.avatarUrl = core.avatarUrl(context.profile.uid) + "?" + Math.random()
    state.isNewUser = context.isNewUser

    let c=0
    state.profile.memberPages.forEach((member)=>{ if(member.alert == true) c++  }) 
    state.countUnreadLiveMsg = c
    

  },
  logout(state, data) {
    state.user.name = null
    state.user.id = null
    state.user.access_token = null
    state.profile.uid = null
    state.profile.avatarUrl = null
    state.profile.step = null
    state.profile.lat = null
    state.profile.lng = null
    state.profile.name = null
    state.profile.pages = null
    state.isAdmin = false
    state.isLoggedIn = false
    if(data && data.loginErrorMsg != null)
      state.loginErrorMsg = data.loginErrorMsg
  },
  changeAvatar(state){
    state.avatarUrl = core.avatarUrl(state.profile.uid) + "?" + Math.random()
  },
  fetchReferrerLoading(state, isLoading) {
    state.referrer.loading = isLoading;
  },
  fetchReferrerError(state, err) {
    state.referrer.error = err;
    state.referrer.profile = null;
  },
  fetchReferrerSuccess(state, referrerProfile) {
    state.referrer.error = null;
    state.referrer.profile = referrerProfile;
  },
  fetchRegisterLoading(state, isLoading) {
    state.register.loading = isLoading;
  },
  fetchRegisterError(state, err) {
    state.register.error = err;
    state.register.success = false;
  },
  fetchRegisterSuccess(state) {
    state.register.error = null;
    state.register.success = true;
  },
  fetchLoginLoading(state, isLoading) {
    state.login.loading = isLoading;
  },
  fetchLoginError(state, err) {
    state.login.error = err;
    state.login.success = false;
    state.isLoggedIn = false;
  },
  fetchLoginSuccess(state) {
    state.login.error = null;
    state.login.success = true;
    state.isLoggedIn = true;
  },
  change_user_step(state, step) {
    state.profile.step = step
  },
  change_admin_state(state, bool) {
    state.isAdmin = bool
  },
  add_page_to_user(state, page) {
    state.user.pages.push(page)
  },
  update_position(state, geoloc) {
    state.profile.coordinates = [geoloc.lng, geoloc.lat]
  },
  update_city_address(state, data) {
    state.profile.city = data.city
    state.profile.address = data.address
  },
  update_hashtags(state, hashtags) {
    state.profile.hashtags = hashtags.hashtags
    state.profile.hashtagsG = hashtags.hashtagsG
    state.profile.hashtagsC = hashtags.hashtagsC
    state.profile.hashtagsM = hashtags.hashtagsM
  },
  update_current_radius(state, radius) {
    state.radius = radius
  },
  update_date_read_notif(state, date) {
    state.profile.dateReadNotif = date
  },
  update_date_read_private(state, date) {
    state.profile.dateReadPrivate = date
  },
  update_is_new_user(state, isNewUser) {
    state.isNewUser = isNewUser
  },
  login_error_msg(state, errorMsg) {
    state.loginErrorMsg = errorMsg
  },

  alert_member_new_livemsg(state, { liveMsg, active }) {
    console.log("#LIVE #AUTH alert_member_new_livemsg ON", liveMsg.parentPageUid, state.profile.memberPages.length, active)
    //core.upAlertLiveOnMember(state, state.currentPage.uid, liveMsg.parentPageUid, active)
    state.profile.memberPages.forEach((friend, f)=>{
      if(friend.type == 'user'){
        //console.log("#LIVE ALERT : SEARCH USER", core.liveUserUid(friend.uid, state.profile.uid), liveMsg)
        if(core.liveUserUid(friend.uid, state.profile.uid) == liveMsg.parentPageUid){
          state.profile.memberPages[f].alert = active
          //console.log("#LIVE ALERT : FOUND USER", core.liveUserUid(friend.uid, state.profile.uid), liveMsg.parentPageUid)
        }
      }else{
          //console.log("#LIVE ALERT : SEARCH NOT USER", friend.uid, liveMsg)
          if(friend.uid == liveMsg.parentPageUid){
            state.profile.memberPages[f].alert = active
            //console.log("#LIVE ALERT : FOUND NOT USER", friend.uid, liveMsg.parentPageUid)
          }
      }
    })
    state.profile.memberPages = core.sortMember(state.profile.memberPages)
    state.lastCheckLiveNotif = new Date()
    
    let c=0
    state.profile.memberPages.forEach((member)=>{ if(member.alert == true) c++  }) 
    state.countUnreadLiveMsg = c
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}