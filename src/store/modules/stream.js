import axios from "axios";

const state = () => ({
  isLoading: false,
  posts: [],
  pages: [],
  countPages: 0,
  mode : 'posts', //posts, pages, send
  origin: 'local',//local, follows, favorites
  pageType: '',   //user, group, assembly, event
  searchStr: '',
  lastStreamLength: 0, //size of the last stream received (to know if its the end)
  lastNbPerPage: 0,
  radiusMax: 0,
  rootMarker: {
    lat: 47.16685851396646,
    lng: -2.1697435379028323,
  },
  rootMarkerInited: false,
  map: {
    userMainPosition: [0, 0],
    show: false
  }
});

const actions = {
  async init({ state, commit }) {
    //if(window.isDev) { console.log('#STREAM init', state.radiusMax) }
    //commit('init', {  userMainPosition: rootState.auth.profile.coordinates })
    
    if(state.radiusMax == 0){
      let radius = 600
      try{
        const res = await axios.get('/instance/get-radius-max')
        if(res.status == 404){
          //console.log("Error : /instance/get-radius-max return null (404). Using default radiusMax value", radius)
        }else{
          radius = res.data.radiusMax
          //console.log("Success : /instance/get-radius-max exist. Using api radiusMax value", radius)
        }
        await commit('setRadiusMax', radius)
      }catch(e){
        await commit('setRadiusMax', radius)
        //console.log("Catch Error : /instance/get-radius-max not exist. Using default radiusMax value", radius)
      }

      //if(window.isDev) { console.log('#STREAM init radius', radius) }
    }

  },
  async fetchPost({ state, commit }, params) {
    try {
      commit('setIsLoading', true);
      //if (state.posts.length !== 0 && !opts.refresh) { return }

      if(window.isDev) { console.log('#STREAM fetchPost params', params) }
      const {data} = await axios.post('/publication/get-stream', params)
      if (data.error) { throw new Error(data.errorMsg) }

      //console.log("AFTER LOGIN ?", params.pageNumber, data.stream)
      if(params.pageNumber == 1) await commit('updatePosts', data.stream)
      else                       await commit('addPosts', data.stream)

      await commit('setLastStreamLength', data.stream.length)
      await commit('setLastNbPerPage', data.nbPerPage)

      if(window.isDev) { console.log('#STREAM fetchPost new state', state.posts.length, state.posts) }
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load stream. Err:', err.message);
      }
    } finally {
      commit("setIsLoading", false);
    }
  },
  async fetchPages({ state, commit }, params) {
    try {
      commit("setIsLoading", true);
      //if (state.pages.length !== 0 && !opts.refresh) { return }

      if(window.isDev) { console.log('#STREAM fetchPages params', params) }
      const {data} = await axios.post('/page/get-map-pages', params)
      if (data.error) { throw new Error(data.errorMsg) }

      //console.log('#STREAM fetchPages params', data.mapPages)

      if(params.pageNumber == 1) await commit('updatePages', data.mapPages)
      else                       await commit('addPages', data.mapPages)

      await commit('updateCountPages', data.count)
      await commit('setLastStreamLength', data.mapPages.length)
      await commit('setLastNbPerPage', data.nbPerPage)

      if(window.isDev) { console.log('#STREAM fetchPages new state', state.pages.length, state.posts) }
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load stream. Err:', err.message);
      }
    } finally {
      commit('setIsLoading', false);
    }
  },
  async fetchCountPages({ state, commit }, params) {
    try {
      commit('setIsLoading', true);
      //if (state.pages.length !== 0 && !opts.refresh) { return }

      if(window.isDev) { console.log('#STREAM fetchCountPages params', params) }
      const {data} = await axios.post('/page/get-count-pages', params)
      if (data.error) { throw new Error(data.errorMsg) }

      await commit('updateCountPages', data.count)
      if(window.isDev) { console.log('#STREAM fetchCountPages new state', state.pages.length, state.posts) }
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load stream. Err:', err.message);
      }
    } finally {
      commit('setIsLoading', false);
    }
  },
  async initPosts({ commit }) {
    if(window.isDev) { console.log('#STREAM initPosts') }
    commit('initPosts')
  },
  async initPages({ commit }) {
    if(window.isDev) { console.log('#STREAM initPages') }
    commit('initPages')
  },
  async setMode({ commit }, mode) {
    if(window.isDev) { console.log('#STREAM setMode', mode) }
    commit('setMode', mode)
  },
  async setOrigin({ commit }, origin) {
    if(window.isDev) { console.log('#STREAM setOrigin', origin) }
    commit('setOrigin', origin)
  },
  async setPageType({ commit }, pageType) {
    if(window.isDev) { console.log('#STREAM setPageType', pageType) }
    commit('setPageType', pageType)
  },
  async setSearchStr({ commit }, searchStr) {
    if(window.isDev) { console.log('#STREAM setSearchStr', searchStr) }
    commit('setSearchStr', searchStr)
  },
  async showMap({ commit }, show) {
    if(window.isDev) { console.log('#STREAM showMap', show) }
    commit('showMap', show)
  },
  async addNewPost({ commit }, post) {
    if(window.isDev) { console.log('#STREAM addNewPost', post) }
    commit('addNewPost', post)
  },
  async setRootMarker({ commit }, marker) {
    if(window.isDev) { console.log('#STREAM setRootMarker', marker) }
    commit('setRootMarker', marker)
  },
  
};

const mutations = {
  setIsLoading: (state, isLoading) => state.isLoading = isLoading,
  init(state, params) {
    state.map.userMainPosition = params.userMainPosition
  },
  initPosts(state) {
    state.posts = []
  },
  initPages(state) {
    state.pages = []
  },
  updatePosts(state, stream) {
    state.posts = stream
  },
  updatePages(state, stream) {
    state.pages = stream
  },
  addPosts(state, stream) {
    state.posts = state.posts.concat(stream)
  },
  addPages(state, stream) {
    state.pages = state.pages.concat(stream)
  },
  updateCountPages(state, count) {
    state.countPages = count
  },
  setMode(state, mode) {
    state.mode = mode
    if(mode == 'send'){
      state.map.show = true
      state.pageType = 'user'
    }
  },
  setOrigin(state, origin) {
    state.origin = origin
  },
  setPageType(state, pageType) {
    state.mode = 'pages'
    state.pageType = pageType
  },
  setSearchStr(state, searchStr) {
    state.searchStr = searchStr
  },
  showMap(state, show) {
    state.map.show = show
  },
  setLastStreamLength(state, len){
    state.lastStreamLength = len
  },
  setLastNbPerPage(state, nbPerPage){
    state.lastNbPerPage = nbPerPage
  },
  addNewPost(state, post){
    state.posts.unshift(post)
  },
  setRootMarker(state, marker){
    state.rootMarker = marker
    state.rootMarkerInited = true
  },
  setRadiusMax(state, radius){
    state.radiusMax = radius
  },

}


// getters
const getters = {
  showStream: (state) => {
   return state.mode != "map"
  },
  showFormEditPost: (state) => {
   return state.mode == "send"
  },
  draggable: (state) => {
   return state.mode != "posts"
  },
  endOfStream: (state) => {
    console.log("endOfStream", state.lastStreamLength, state.lastNbPerPage)
    return state.lastStreamLength < state.lastNbPerPage
   },
}


export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
}
