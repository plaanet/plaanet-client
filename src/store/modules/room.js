const axios = require('axios')
const core = require('../../plugins/core.js').default

const state = () => ({
  roomUid: null,
  surveys: [],
  page: null,
  currentSurvey: null
});

const actions = {
  
  async fetchRoom({ commit }, params) {
    let resPage = { data: { page: null }}
    
    if(params.roomUid != null)
    resPage = await axios.post(`/page/profil`, { pageUid: params.roomUid })
    
    const res = await axios.post('/room/get-surveys', params)
    if (res.data.error == false) {
      console.log("#ROOM fetchRoom", res.data)
      commit('initRoom', { roomUid: params.roomUid, 
                           page: resPage.data.page, 
                           data: res.data })
    }
  },
  async fetchCurrentSurvey({ commit }, surveyUid) {
    const res = await axios.get(`/room/get-survey/${surveyUid}`)
    if (res.data.error == false) {
      console.log("#ROOM fetchCurrentSurvey", res.data)
      commit('setCurrentSurvey', res.data )
    }
  },
  async refreshSurvey({ state, commit }, surveyUid) {
    state.surveys.forEach(async (survey, i) => {
      if(surveyUid == survey.uid){
        const res = await axios.get(`/room/get-survey/${surveyUid}`)
        if (res.data.error == false) {
          console.log("#ROOM fetchCurrentSurvey", res.data)
          commit('refreshSurvey', { i: i, survey: res.data.survey } )
        }
      }
    })
  },
  addCommentToCurrentSurvey({ commit }, comment) {
    console.log("#ROOM addCommentToCurrentSurvey", comment)
    commit('addCommentToCurrentSurvey', comment)
  },
  onNotifEmoji({ commit }, notif) {
    commit('onNotifEmoji', notif)
  },
  onNotifComment({ commit }, notif) {
    commit('onNotifComment', notif)
  },
  deleteCurrentSurvey({ commit }){
    commit('deleteCurrentSurvey')
  }
};

const mutations = {
  initRoom(state, { roomUid, page, data }) {
    state.roomUid = roomUid
    state.page = page
    state.surveys = data.surveys
  },
  setCurrentSurvey(state, data) {
    console.log('setCurrentSurvey', data)
    state.currentSurvey = data.survey;
    state.currentSurvey.post = data.post
  },
  deleteCurrentSurvey(state, data) {
    console.log('setCurrentSurvey', data)
    state.currentSurvey = null
  },
  refreshSurvey(state, { i, survey }){
    state.surveys[i] = survey
  },
  addCommentToCurrentSurvey(state, comment) {
    state.currentSurvey.post.comments.push(comment)
  },
  onNotifEmoji(state, notif) {
    console.log('onNotifEmoji', core)
    state.currentSurvey.post = core.addEmojiToPost(notif, state.currentSurvey.post)
  },
  onNotifComment(state, notif) {
    console.log("onNotifComment", state, notif)
    const comments = core.addCommentToPost(notif, state.currentSurvey.post).comments
    state.currentSurvey.comments = comments
    console.log("onNotifComment", state.currentSurvey.comments.length)
  },
};

const getters = {
  shouldShowLinkPreviews(state) {
    if (state.linkPreviewsLoading) {
      return false;
    }

    return state.showLinkPreviews;
  },
  linkPreviewsList(state) {
    return Object.values(state.linkPreviews);
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}