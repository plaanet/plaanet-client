import axios from "axios";
import core from "../../plugins/core";

const state = () => ({
  isLoading: true,
  loadingChannel: false,
  profileUid: null,
  
  currentPage: null,
  currentDestPage: null,
  currentLive: null,
  currentCategoryUid: null,
  currentChannelUid: null,
  currentConversation: [],
  lastMsgDate: null,
  lastMsgNotRead: null,
  refreshConvDate: null,
  refreshOnlineDate: null,

  convLimit: 49,
  convSkip: 0

});

const actions = {
  
  async fetchLive({ dispatch, state, rootState, commit }, params) {
    try {
      if(state.isLoading) return

      commit('setLoadingState', true);
      commit('setProfileUid', rootState.auth.profile.uid)

      if(window.isDev) { console.log('#LIVE fetchLive params', params) }
      const {data} = await axios.post('/live/open-live-stream', params)
      if (data.error) { throw new Error(data.errorMsg) }
      if(window.isDev) { console.log('#LIVE data.live XXX', data.live) }

      if(data.page.type != "user" || data.page.uid == rootState.auth.profile.uid)
            await commit('setCurrentPage', data.page)
      else{
        await commit('setCurrentPage', rootState.auth.profile)
        await commit('setCurrentDestPage', data.page)
      }  
      console.log("data.live", data.live, rootState.auth.profile.uid)
      if(data.live == null){
        await commit('setCurrentChannel', { channelUid: null, categoryUid: null })
        await commit('setCurrentConversation', [])
        return null
      }

      await commit('setCurrentLive', data.live)
      //{ lastOpenCategoryUid, lastOpenChannelUid }

      let chanUid = (params.keepSameChannel) ? state.currentChannelUid : null
      let catUid = (params.keepSameChannel) ? state.currentCategoryUid : null

      if(params.categoryUid != null && params.channelUid != null){
        chanUid = params.channelUid
        catUid = params.categoryUid
      }else{
        let { lastOpenCategoryUid, lastOpenChannelUid } = core.getLastOpenChannelUid(rootState.auth.profile.uid, data.live.categories)
        console.log("lastOpenChannelUid", lastOpenCategoryUid, lastOpenChannelUid)
        
        chanUid = (params.keepSameChannel) ? state.currentChannelUid : lastOpenChannelUid
        catUid = (params.keepSameChannel) ? state.currentCategoryUid : lastOpenCategoryUid
      }

      await dispatch('live/openChannel', { liveUid: data.live.uid, 
                                           categoryUid: catUid, 
                                           channelUid: chanUid }, 
                                    { root: true })
      
      if(window.isDev) { console.log('#LIVE fetchLive new state', state.currentPage.name, data.live ) }
      return data.live 
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load live. Err:', err.message);
      }
    } finally {
      commit("setLoadingState", false);

      if(state.currentPage != null)
      await dispatch('whoIsOnline', { root: true })
    }
  },
  async sendMessage({ commit }, params) {
    try {
      commit('setLoadingState', true);
      if(window.isDev) { console.log('#LIVE sendMessage params', params) }
      const {data} = await axios.post('/live/send-message', params)
      if (data.error) { throw new Error(data.errorMsg) }

      await commit('addMessageToChannel', data.liveMsg)

      if(window.isDev) { console.log('#LIVE sendMessage ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      commit("setLoadingState", false);
    }
  },
  async openChannel({ dispatch, state, commit }, params) {
    try {
      commit('setLoadingState', true)
      commit('setLoadingChannel', true)
      commit('initConversation')
      
      if(window.isDev) { console.log('#LIVE openChannel params', params) }
      const {data} = await axios.post('/live/open-channel', params)
      if (data.error) { throw new Error(data.errorMsg) }

      if(window.isDev) { console.log('#LIVE openChannel data', data, params) }
      await commit('setCurrentChannel', params)
      await commit('setCurrentConversation', data.conversation)

      if(window.isDev) { console.log('#LIVE openChannel ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      let LM = { categoryUid: state.currentCategoryUid,
                  channelUid: state.currentChannelUid,
                  parentPageUid: state.currentLive.parentPageUid }

      commit('alertMsgOnChannel', { liveMsg : LM, 
                                    active: false,
                                    mentions: []
                                  })
      
      await dispatch('auth/alertMemberNewLivemsg', { liveMsg: LM, active: false }, { root: true })

      commit('setLoadingChannel', false);
      commit("setLoadingState", false);
    }
  },
  
  async loadMoreConversation({ state, commit }, params) {
    try {
      commit('setLoadingState', true)
      commit('incConvSkip')
      if(window.isDev) { console.log('#LIVE loadMoreConversation params') }
      const {data} = await axios.post('/live/get-conversation', {
                              liveUid: state.currentLive.uid,
                              categoryUid: state.currentCategoryUid,
                              channelUid: state.currentChannelUid,
                              limit: state.convLimit,
                              skip: state.convSkip
                            })
      if (data.error) { throw new Error(data.errorMsg) }

      if(window.isDev) { console.log('#LIVE loadMoreConversation data', data, params) }
      await commit('addToConversation', data.conversation)

      if(window.isDev) { console.log('#LIVE loadMoreConversation ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      //commit('setLoadingChannel', false);
      commit("setLoadingState", false);
    }
  },
  async refreshConv({ commit }, params) {
    try {
      commit('setLoadingState', true);
      //commit('setLoadingChannel', true);
      if(window.isDev) { console.log('#LIVE openChannel params', params) }
      const {data} = await axios.post('/live/open-channel', params)
      if (data.error) { throw new Error(data.errorMsg) }

      if(window.isDev) { console.log('#LIVE openChannel data', data, params) }
      //await commit('setCurrentChannel', params)
      await commit('setCurrentConversation', data.conversation)

      if(window.isDev) { console.log('#LIVE openChannel ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      //commit('setLoadingChannel', false);
      commit("setLoadingState", false);
    }
  },
  async createChannel({ dispatch, commit }, params) {
    try {
      commit('setLoadingState', true);
      if(window.isDev) { console.log('#LIVE createChannel params', params) }
      const {data} = await axios.post('/live/create-channel', params)
      if (data.error) { throw new Error(data.errorMsg) }

      await commit('setCurrentLive', data.live)
      await commit('setCurrentChannel', { categoryUid: params.categoryUid, 
                                          channelUid: data.newChannelUid })

      await dispatch('live/openChannel', { liveUid: data.live.uid, 
                                            categoryUid: params.categoryUid, 
                                            channelUid: data.newChannelUid }, 
                                        { root: true })

      if(window.isDev) { console.log('#LIVE createChannel ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      commit("setLoadingState", false);
    }
  },
  async createCategory({ commit }, params) {
    try {
      commit('setLoadingState', true);
      if(window.isDev) { console.log('#LIVE createCategory params', params) }
      const {data} = await axios.post('/live/create-category', params)
      if (data.error) { throw new Error(data.errorMsg) }

      await commit('setCurrentLive', data.live)

      if(window.isDev) { console.log('#LIVE createCategory ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } finally {
      commit("setLoadingState", false);
    }
  },
  
  async sockReceiveLiveMsg({ dispatch, state, commit }, { liveMsg, mentions }) {
    try {
      if(window.isDev) { console.log('#LIVE sockReceiveLiveMsg', liveMsg, mentions) }
      
      if(state.currentLive != null && liveMsg.liveUid == state.currentLive.uid){
        if(liveMsg.categoryUid == state.currentCategoryUid){
          if(liveMsg.channelUid == state.currentChannelUid){
            commit('addMessageToChannel', liveMsg)
          }else{
            commit('alertMsgOnChannel', { liveMsg: liveMsg, mentions: mentions, active: true })
          }
        }else{
          commit('alertMsgOnChannel', { liveMsg: liveMsg, mentions: mentions, active: true })
        }
      }else{
        commit('alertMsgOnChannel', { liveMsg: liveMsg, mentions: mentions, active: true })
        dispatch('auth/alertMemberNewLivemsg', { liveMsg: liveMsg, active: true }, { root: true })
        //core.showPushNotif({ verb: "NEW_PRIVATE_MSG", authors: [ { name: liveMsg.author.name } ]})
      }

      
      
      if(window.isDev) { console.log('#LIVE sendMessage ok') }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } 
  },
  
  async sockEditedLiveMsg({ state }, { liveMsg }) {
    try {
      if(window.isDev) { console.log('#LIVE sockEditedLiveMsg', liveMsg) }
      
      if(state.currentLive != null && liveMsg.liveUid == state.currentLive.uid){
        if(liveMsg.categoryUid == state.currentCategoryUid){
          if(liveMsg.channelUid == state.currentChannelUid){
            state.currentConversation.forEach((msg, m)=>{
              if(msg.uid == liveMsg.uid){
                state.currentConversation[m] = liveMsg
                state.refreshConvDate = new Date()
                //console.log("editMessageToChannel?", state.currentConversation[m])
              }
            })
          }
        }
      }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } 
  },
  async sockDeletedLiveMsg({ state }, { liveMsg }) {
    try {
      if(window.isDev) { console.log('#LIVE sockDeletedLiveMsg', liveMsg) }
      
      if(state.currentLive != null && liveMsg.liveUid == state.currentLive.uid){
        if(liveMsg.categoryUid == state.currentCategoryUid){
          if(liveMsg.channelUid == state.currentChannelUid){
            console.log("deletedMessage?", state.currentConversation.length)
            state.currentConversation.forEach((msg, m)=>{
              if(msg.uid == liveMsg.uid){
                state.currentConversation.splice(m, 1)
                state.refreshConvDate = new Date()
                console.log("deletedMessage?", state.currentConversation[m])
              }
            })
            console.log("deletedMessage?", state.currentConversation.length)
          }
        }
      }
    } catch (err) {
      if(window.isDev) console.log('Cannot load live. Err:', err.message);
    } 
  },
  setLiveLoading({ commit }, isLoading) {
    commit('setLoadingState', isLoading);
  },

  async whoIsOnline({ rootState, state }){
    const {data} = await axios.post('/live/who-is-online', { pageUid: state.currentPage.uid })
    if (data.error) { throw new Error(data.errorMsg) }
    //re-init all
    state.currentPage.roles.editor.forEach((e, x)=>{ e.uid;
      data.connected.forEach(()=>{ state.currentPage.roles.editor[x].online = false })
    })
    rootState.auth.profile.memberPages.forEach((e, x)=>{ e.uid;
      data.connected.forEach(()=>{ rootState.auth.profile.memberPages[x].online = false })
    })

    state.currentPage.roles.editor.forEach((e, x)=>{
      data.connected.forEach((c)=>{ if(e.uid == c) state.currentPage.roles.editor[x].online = true })
    })
    rootState.auth.profile.memberPages.forEach((e, x)=>{
      data.connected.forEach((c)=>{ if(e.uid == c) rootState.auth.profile.memberPages[x].online = true })
    })
    state.refreshOnlineDate = new Date()
    //console.log("state", state)
  }
};

const mutations = {
  setLoadingState(state, isLoading) {
    state.isLoading = isLoading;
  },
  setLoadingChannel(state, isLoading) {
    state.loadingChannel = isLoading;
  },
  setCurrentPage(state, page) {
    state.currentPage = page;
    state.currentDestPage = null;
  },
  setCurrentDestPage(state, page) {
    state.currentDestPage = page;
  },
  setProfileUid(state, uid) {
    state.profileUid = uid;
  },
  setCurrentLive(state, live) {
    console.log("setCurrentLive", live)
    state.currentLive = live;
    //affiche les notifs sur les channels
    state.currentLive.categories.forEach((category, j)=>{
        category.channels.forEach((channel, i)=>{
            console.log("1update dateLastOpen?", state.currentLive.categories[j].channels[i].dateLastOpen)
            if(state.currentLive.categories[j].channels[i].dateLastOpen == null) 
              state.currentLive.categories[j].channels[i].dateLastOpen = []

              let found = false
              channel.dateLastOpen.forEach((dataOpen)=>{
                if(dataOpen.uid == state.profileUid){
                  //check if there is new message
                  state.currentLive.categories[j].channels[i].alert = (dataOpen.date < channel.dateLastMsg)

                  //find count mentions
                  if(state.currentLive.categories[j].channels[i].countMention == null)
                    state.currentLive.categories[j].channels[i].countMention = 0

                  state.currentLive.categories[j].channels[i].countMention = core.countMention(channel, state.profileUid)
                  found = true
                }
              })
              if(!found){
                state.currentLive.categories[j].channels[i].dateLastOpen.push({
                      uid: state.profileUid,
                      date: new Date()
                  })
              }
        })
    })
  },
  setCurrentConversation(state, conv) {
    state.currentConversation = conv;
  },
  setCurrentChannel(state, data) {
    console.log("setCurrentChannel", data)
    state.currentChannelUid = data.channelUid;
    state.currentCategoryUid = data.categoryUid;

    state.currentLive.categories.forEach((category, j)=>{
      if(category.uid == state.currentCategoryUid){
        category.channels.forEach((channel, i)=>{
          //console.log("3update dateLastOpen? alert", state.currentLive.categories[j].channels[i].alert, i)
          if(channel.uid == state.currentChannelUid){
            //console.log("4update dateLastOpen?", state.currentLive.categories[j].channels[i].dateLastOpen, i)
            if(state.currentLive.categories[j].channels[i].dateLastOpen == null) 
              state.currentLive.categories[j].channels[i].dateLastOpen = []
              let found = false
              channel.dateLastOpen.forEach((dataOpen, x)=>{
                if(dataOpen.uid == state.profileUid){
                  state.currentLive.categories[j].channels[i].dateLastOpen[x].date = new Date()
                  found = true
                }
              })
              if(!found){
                state.currentLive.categories[j].channels[i].dateLastOpen.push({
                      uid: state.profileUid,
                      date: new Date()
                  })
              }
              state.currentLive.categories[j].channels[i].alert = false
              state.currentLive.categories[j].channels[i].countMention = 0
          }
        })
      }
    })
      
    console.log("setCurrentChannel new state", state.currentChannelUid, state.currentCategoryUid)
  },
  addMessageToChannel(state, liveMsg) {
    state.currentLive.categories.forEach((category, j)=>{
      if(category.uid == liveMsg.categoryUid){
        category.channels.forEach((channel, i)=>{
          if(channel.uid == liveMsg.channelUid){
            console.log("addMessageToChannel?", state.currentLive.categories[j].channels[i])
            state.currentConversation.push(liveMsg)
            state.currentLive.categories[j].channels[i].dateLastMsg = new Date()

          }
        })
      }
    })
  },

  alertMsgOnChannel(state, { liveMsg, mentions, active }) {
    console.log("#LIVE alertMsgOnChannel ON", state)
    if(state.currentLive != null && state.currentPage.type != 'user'){
      state.currentLive.categories.forEach((category, j)=>{
        if(category.uid == liveMsg.categoryUid){
          category.channels.forEach((channel, i)=>{
            if(channel.uid == liveMsg.channelUid){
              //console.log("alertMsgOnChannel", state.currentLive.categories[j].channels[i].name, active)
              
              //add new mentions
              if(state.currentLive.categories[j].channels[i].mentions == null)
                state.currentLive.categories[j].channels[i].mentions = []

              console.log("**countMention", mentions.length, mentions)
              mentions.forEach((mention)=>{
                state.currentLive.categories[j].channels[i].mentions.push(mention)
              })
              
              if(state.currentLive.categories[j].channels[i].countMention == null)
                state.currentLive.categories[j].channels[i].countMention = 0
              state.currentLive.categories[j].channels[i].countMention = core.countMention(state.currentLive.categories[j].channels[i], state.profileUid)
              
              state.currentLive.categories[j].channels[i].alert = active
              //state.currentLive.categories[j].channels[i].dateLastMsg = new Date()
              state.lastMsgDate = new Date()

            }
          })
        }
      })
    }
    if(active == true){
      state.lastMsgNotRead = liveMsg
    }
    console.log("#LIVE ALERT ON USER CONVERSATION", liveMsg.parentPageUid)
  },
  initConversation(state) {
    //state.convLimit = 5
    state.convSkip = 0
  },
  incConvSkip(state){
    state.convSkip = state.convSkip + 1
  },
  addToConversation(state, conv){
    state.currentConversation = conv.concat(state.currentConversation)
  }

};

const getters = {};


export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
}
