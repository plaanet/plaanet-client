import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import axios from 'axios'

import store from '../store'
import routes from './routes'

import NProgress from 'nprogress'
NProgress.configure({ showSpinner: false });
const config = require('../config/' + process.env.NODE_ENV)

//import Home from '../views/home.vue'
//import Login from '../viewslogin.vue'
//import Register from '../views/register.vue'

Vue.use(VueRouter)
Vue.use(Vuex)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

// FETCH NETWORK BEFORE EACH ROUTES.
router.beforeEach(async (to, from, next) => {
  // always check if network exists in store
  //if not, get it
  if (store.state.network.nodes.length == 0) {
    await store.dispatch('network/fetchNodes', null, { root: true });
  }
  
  if(localStorage.getItem("updateAppVersion") != null && config.mobileAppVersion != null && config.mobileAppVersion != ""){
    if(localStorage.getItem("updateAppVersion") != config.mobileAppVersion){
      console.log("router.beforeEach", to.path)
      if(to.path != "/last-version-mobile-app" && to.path != "/admin/version-mobile")
      return next({ path: '/last-version-mobile-app' });
    }
  }

  next();
});

// STORE REFERRER UID TO LS.
router.beforeEach(async (to, from, next) => {
  // Check and save referrer username.
  if (to.query && to.query.ref) {
    localStorage.setItem("referrerUid", to.query.ref);
  }

  next();
});

// AUTH GUARD.  
router.beforeEach(async (to, from, next) => {
  // This route is only for guests, if logged redirect to /stream.
  if (
    store.state.auth.isLoggedIn &&
    to.matched.some(route => route.meta.guestOnly)
  ) {
    return next({ path: '/stream' });
  }
  // this route requires auth, check if logged in
  else if (
    store.state.auth.isLoggedIn === false &&
    to.matched.some(route => route.meta.requiresAuth)
  ) {
    // Logged out, trying to access a requiresAuth route
    // Redirect to /login
    return next({ path: '/login' });
  } else {
    //if you want to go to an open route
    return next(); // make sure to always call next()!
  }
});


router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    NProgress.start()
  }
  next()
})

router.afterEach(() => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})



axios.interceptors.request.use(
  (config) => {
    let token = store.state.auth.user.access_token;
    if (token) {
      config.headers["x-auth-token"] = token;
    }
    NProgress.start()
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);



// before a response is returned stop nprogress
axios.interceptors.response.use(response => {
  NProgress.done()
  return response
})

export default router;


