import Axios from "axios";
import Vue from "vue";
import Vuex from "vuex";
import VueCookies from 'vue-cookies'
import VueClipboard from "vue-clipboard2";
//import VueNativeNotification from 'vue-native-notification'

import I18NError from './plugins/i18nError';
import vuetify from "./plugins/vuetify";
import router from "./router/router";
import store from "./store";

import App from "./App.vue";

if (
  window.webpackHotUpdate ||
  (process.env.NODE_ENV !== "production" &&
    process.env.NODE_ENV !== "test" &&
    typeof console !== "undefined")
) {
  window.isDev = true;
}

async function main() {
  Axios.defaults.validateStatus = function() {
    return true;
  };

  Axios.defaults.withCredentials = true;
  Vue.config.productionTip = false;
  
  Vue.use(Vuex);
  Vue.use(VueCookies);
  Vue.use(VueClipboard);
  Vue.use(I18NError);

  // Vue.use(VueNativeNotification, {
  //   // Automatic permission request before showing notification (default: true)
  //   requestOnNotify: true
  // })
  
  await store.dispatch('network/init', null, { root: true });
  await store.dispatch('auth/checkAuth', null, { root: true });
  //await store.dispatch('notifications/init', null, { root: true });
      
  new Vue({
    router,
    vuetify,
    store,
    render: h => h(App),
  }).$mount('#app');
      
}
    
main();
